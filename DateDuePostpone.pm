package Koha::Plugin::Com::RBitTechnology::DateDuePostpone;

use Modern::Perl;
use base qw(Koha::Plugins::Base);
use utf8;
use C4::Context;
use Koha::Patrons;
use Koha::List::Patron;
use Koha::Patron::Categories;
use Koha::Database;
use List::MoreUtils;
use Koha::DateUtils qw( dt_from_string output_pref );

use Data::Dumper;

our $VERSION = "2.0.0";

our $metadata = {
    name            => 'Posun termínu výpůjček',
    author          => 'R-Bit Technology, s.r.o.',
    description     => 'Tento nástroj umožňuje v případně neplánovaného uzavření knihovny hromadně posunout expiraci výpůjček na vhodný otvírací den knihovny',
    date_authored   => '2020-03-12',
    date_updated    => '2022-10-01',
    minimum_version => '21.11',
    # maximum_version => undef,
    version         => $VERSION
};

our $independentBranches = C4::Context->preference('IndependentBranches') ne '0';

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    ## Here, we call the 'new' method for our base class
    ## This runs some additional magic and checking
    ## and returns our actual $self
    my $self = $class->SUPER::new($args);

    return $self;
}

sub install() {
    my ( $self, $args ) = @_;
		return 1;
}

sub uninstall() {
    my ( $self, $args ) = @_;
		return 1;
}

sub tool {
    my ( $self, $args ) = @_;

    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('phase') ) {;
        $self->tool_period();
    }
    elsif ( $cgi->param('phase') eq 'issues' ) {
       $self->tool_issues();
    }
    elsif ( $cgi->param('phase') eq 'postpone' ) {
       $self->tool_postpone();
    }
}

sub tool_period {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-period.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my @branches;

    if ($independentBranches) {
        my $branch = Koha::Libraries->find(C4::Context->userenv->{'branch'});
        $template->param(
            branches => \@branches,
            mybranch => $branch
        );
    }
    else {
        @branches = Koha::Libraries->search( {}, { order_by => ['branchname'], columns => [qw/branchcode branchname/] } );
        $template->param(
            branches => \@branches,
            mybranch => ''
        );
    }

    print $template->output();
}

sub tool_issues {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-issues.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my $date_from;
    my $date_to;
    my $branch = '';
    my $branchcode = '';

    if ( $cgi->param('date-from') ) {
        $date_from = output_pref({ dt => Koha::DateUtils::dt_from_string( scalar $cgi->param('date-from') ), dateformat => 'iso', dateonly => 1 });
    }
    if ( $cgi->param('date-to') ) {
        $date_to = output_pref({ dt => dt_from_string( scalar $cgi->param('date-to') ), dateformat => 'iso', dateonly => 1 });
    }

    my @branchcodes = $cgi->multi_param('branch');
    my @branches = Koha::Libraries->search( { branchcode => { -in => \@branchcodes } }, { order_by => ['branchname'], columns => [qw/branchcode/] } );
    my ($totals) = $self->get_totals($date_from, $date_to, @branchcodes);

    $template->param(
        from => scalar $cgi->param("date-from"),
        to => scalar $cgi->param("date-to"),
        issues => $totals->{issues},
        holds => $totals->{holds},
        queue => $totals->{queue},
        branches => \@branches
    );

    print $template->output();

}

sub tool_postpone {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-postpone.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my $date_due;
    my $date_from;
    my $date_to;
    my @branchcodes;

    if ( $cgi->param('date-from') ) {
        $date_from = output_pref({ dt => dt_from_string( scalar $cgi->param('date-from') ), dateformat => 'iso', dateonly => 1 });
    }
    if ( $cgi->param('date-to') ) {
        $date_to = output_pref({ dt => dt_from_string( scalar $cgi->param('date-to') ), dateformat => 'iso', dateonly => 1 });
    }
    if ( $cgi->param('date-due') ) {
        $date_due = output_pref({ dt => dt_from_string( scalar $cgi->param('date-due') ), dateformat => 'iso', dateonly => 1 });
    }
    if ( $cgi->multi_param('branch') ) {
        @branchcodes = $cgi->multi_param('branch');
    }

    my $notifications = 0;
    if ($cgi->param('queue-check') && (scalar $cgi->param('queue-check') == 1)) {
        $notifications = $self->purge_queue(
            $date_from,
            $date_to,
            \@branchcodes
        );
    }

    my $sent_sms = 0;
    if ($cgi->param('sms-check') && (scalar $cgi->param('sms-check') == 1)) {
        $sent_sms = $self->send_sms(
            $date_from,
            $date_to,
            \@branchcodes,
            scalar $cgi->param('sms-text')
        );
    }

    my $sent_email = 0;
    if ($cgi->param('email-check') && (scalar $cgi->param('email-check') == 1)) {
        $sent_email = $self->send_email(
            $date_from,
            $date_to,
            \@branchcodes,
            scalar $cgi->param('email-subject'),
            scalar $cgi->param('email-text')
        );
    }

    $self->update_issues(
        $date_from,
        $date_to,
        $date_due,
        \@branchcodes
    );
    $self->update_holds(
        $date_from,
        $date_to,
        $date_due,
        \@branchcodes
    );

    my ($totals) = $self->get_totals($date_from, $date_to, @branchcodes);

    $template->param(
        from => scalar $cgi->param("date-from"),
        to => scalar $cgi->param("date-to"),
        date_due => scalar $cgi->param("date-due"),
        issues => $totals->{issues},
        holds => $totals->{holds},
        sent_email => $sent_email + 0,      # may return 0E0 as no records, but still success
        sent_sms => $sent_sms + 0,          # may return 0E0 as no records, but still success
        notifications => $notifications + 0 # may return 0E0 as no records, but still success
    );

    print $template->output();

}

sub subtotal {
    my ( $self, $query, @bindParams ) = @_;

    my $dbh = C4::Context->dbh;

    # print $query . "<br>";
    # print Dumper(@bindParams);

    my $sth = $dbh->prepare($query);
    for my $i (0 .. $#bindParams) {
        $sth->bind_param($i + 1, $bindParams[$i]);
    }
    $sth->execute();
    my $row = $sth->fetchrow_hashref();

    return $row->{cnt};
}

sub prepare_branches {
    my ( $self, @branches ) = @_;

    my @qArgs;
    my @bindParams;
    foreach my $branch ( @branches ) {
        push( @bindParams, $branch );
        push( @qArgs, '?');
    }
    my $whereBranch = ' AND branchcode IN (' . join(',', @qArgs) . ')';

    return ($whereBranch, \@bindParams)
}

sub get_totals {
    my ( $self, $date_from, $date_to, @branches ) = @_;

    my $dbh = C4::Context->dbh;

    my $result = {
        issues => {},
        holds => {},
        queue => {}
    };

    my ($whereBranch, $bindParams) = $self->prepare_branches(@branches);
    my @args;

    # issues/holds BEFORE
    @args = ($date_from);
    push(@args, @$bindParams);
    $result->{issues}->{before} = $self->subtotal("SELECT count(*) as cnt FROM issues WHERE DATE(date_due) < ? $whereBranch", @args);
    $result->{holds}->{before} = $self->subtotal("SELECT count(*) as cnt FROM reserves WHERE DATE(waitingdate) < ? $whereBranch", @args);

    # issues/holds AFTER
    @args = ($date_to);
    push(@args, @$bindParams);
    $result->{issues}->{after} = $self->subtotal("SELECT count(*) as cnt FROM issues WHERE DATE(date_due) > ? $whereBranch", @args);
    $result->{holds}->{after} = $self->subtotal("SELECT count(*) as cnt FROM reserves WHERE DATE(waitingdate) > ? $whereBranch", @args);

    # issues/holds BETWEEN
    @args = ($date_from, $date_to);
    push(@args, @$bindParams);
    $result->{issues}->{between} = $self->subtotal("SELECT count(*) as cnt FROM issues WHERE DATE(date_due) BETWEEN ? AND ? $whereBranch", @args);
    $result->{holds}->{between} = $self->subtotal("SELECT count(*) as cnt FROM reserves WHERE DATE(waitingdate) BETWEEN ? AND ? $whereBranch", @args);

    # message_queue BETWEEN
    my $query = "SELECT letter_code, IFNULL(name, '(neuvedeno)') as name, count(*) as count
      FROM message_queue
      LEFT JOIN letter ON letter_code = code AND message_queue.message_transport_type = letter.message_transport_type
      WHERE	STATUS='pending' AND date(time_queued) BETWEEN ? AND ?
      AND borrowernumber IN (
          SELECT borrowernumber FROM issues WHERE DATE(date_due) BETWEEN ? AND ? $whereBranch
          UNION
          SELECT borrowernumber FROM reserves WHERE DATE(waitingdate) BETWEEN ? AND ? $whereBranch
      )
      GROUP BY letter_code";
    @args = ($date_from, $date_to, $date_from, $date_to);
    push(@args, @$bindParams);
    push(@args, $date_from);
    push(@args, $date_to);
    push(@args, @$bindParams);
    my $sth = $dbh->prepare($query);
    for my $i (0 .. $#args) {
        $sth->bind_param($i + 1, $args[$i]);
    }
    $sth->execute();

    my @letters = ();
    while (my $row = $sth->fetchrow_hashref()) {
        push(@letters, $row);
    }
    $result->{queue} = \@letters;
    # print Dumper($result);

    return $result;
}

sub update_issues {
    my ( $self, $date_from, $date_to, $date_due, $branchcodes ) = @_;

    my ($whereBranch, $bindParams) = $self->prepare_branches(@$branchcodes);

    my $dbh = C4::Context->dbh;
    my $query = "UPDATE issues SET date_due = ? WHERE DATE(date_due) BETWEEN ? AND ? $whereBranch";
    my $sth = $dbh->prepare($query);
    my @args = ("$date_due 23:59:00", $date_from, $date_to);
    push(@args, @$bindParams);
    for my $i (0 .. $#args) {
       $sth->bind_param($i + 1, $args[$i]);
    }
    $sth->execute();
}

sub update_holds {
    my ( $self, $date_from, $date_to, $date_due, $branchcodes ) = @_;

    my ($whereBranch, $bindParams) = $self->prepare_branches(@$branchcodes);

    my $dbh = C4::Context->dbh;
    my $query = "UPDATE reserves SET waitingdate = ? WHERE DATE(waitingdate) BETWEEN ? AND ? $whereBranch";
    my $sth = $dbh->prepare($query);
    my @args = ($date_due, $date_from, $date_to);
    push(@args, @$bindParams);
    for my $i (0 .. $#args) {
       $sth->bind_param($i + 1, $args[$i]);
    }
    $sth->execute();
}

sub enqueue_notification {
    my ( $self, $date_from, $date_to, $branchcodes, $subject, $message, $transport) = @_;

    my ($whereBranch, $bindParams) = $self->prepare_branches(@$branchcodes);

    my $dbh = C4::Context->dbh;
    my $query = "INSERT INTO message_queue(borrowernumber, `subject`, content, letter_code, message_transport_type, `status`, time_queued, from_address, to_address)
    SELECT DISTINCT
      borrowernumber,
      ? as subject,
      ? as content,
      'PLUGIN_DATEDUE' as letter_code,
      ? as message_transport_type,
      'pending' as status,
      now() as time_queued,
      IF(branchemail IS NULL OR trim(branchemail) = '', (SELECT value FROM systempreferences WHERE variable = 'KohaAdminEmailAddress'), branchemail) as from_address,
      " . ($transport eq 'sms' ? 'NULL' : 'borrowers.email') . " as to_address
    FROM (
      SELECT borrowernumber FROM issues WHERE DATE(date_due) BETWEEN ? AND ? $whereBranch
      UNION
      SELECT borrowernumber FROM reserves WHERE DATE(waitingdate) BETWEEN ? AND ? $whereBranch
      ) T
      JOIN borrowers USING(borrowernumber)
      JOIN branches USING(branchcode) ";
    if ($transport eq 'sms') {
        $query .= "WHERE borrowers.smsalertnumber IS NOT NULL AND trim(borrowers.smsalertnumber) != '';"
    } elsif ($transport eq 'email') {
        $query .= "WHERE borrowers.email IS NOT NULL AND trim(borrowers.email) != '';"
    }
    my $sth = $dbh->prepare($query);
    my @args = ($subject, $message, $transport, $date_from, $date_to);
    push(@args, @$bindParams);
    push(@args, $date_from);
    push(@args, $date_to);
    push(@args, @$bindParams);
    for my $i (0 .. $#args) {
       $sth->bind_param($i + 1, $args[$i]);
    }

    return $sth->execute();
}

sub send_sms {
    my ( $self, $date_from, $date_to, $branchcodes, $message ) = @_;

    return $self->enqueue_notification($date_from, $date_to, $branchcodes, 'posunutí termínu výpůjčky', $message, 'sms');
}

sub send_email {
    my ( $self, $date_from, $date_to, $branchcodes, $subject, $message ) = @_;

    return $self->enqueue_notification($date_from, $date_to, $branchcodes, $subject, $message, 'email');
}

sub purge_queue {
    my ( $self, $date_from, $date_to, $branchcodes ) = @_;

    my ($whereBranch, $bindParams) = $self->prepare_branches(@$branchcodes);

    my $dbh = C4::Context->dbh;
    my $query = "DELETE FROM message_queue WHERE STATUS='pending' AND date(time_queued) BETWEEN ? AND ?
        AND borrowernumber IN (
          SELECT borrowernumber FROM issues WHERE DATE(date_due) BETWEEN ? AND ? $whereBranch
          UNION
          SELECT borrowernumber FROM reserves WHERE DATE(waitingdate) BETWEEN ? AND ? $whereBranch
        )";
    my $sth = $dbh->prepare($query);

    my @args = ($date_from, $date_to, $date_from, $date_to);
    push(@args, @$bindParams);
    push(@args, $date_from);
    push(@args, $date_to);
    push(@args, @$bindParams);
    for my $i (0 .. $#args) {
       $sth->bind_param($i + 1, $args[$i]);
    }

    return $sth->execute();
}


1;
